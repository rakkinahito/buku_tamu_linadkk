<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


Route::controller(App\Http\Controllers\PagesController::class)
    ->as('page.')
    ->group(function () {
        Route::get('/', 'index')->name('home');
        Route::get('/about', 'about')->name('about');
        Route::get('/contact', 'contact')->name('contact');
        Route::get('/ulasan/{jenisUlasan}', 'ulasan')->name('ulasan');
        Route::post('/ulasan/{jenisUlasan}', 'ulasanPost')->name('ulasan.post');
    });

Route::controller(App\Http\Controllers\MenuController::class)
    ->as('menu.')
    ->prefix('menu')
    ->group(function () {
        Route::get('/', 'menu')->name('menu');
        Route::get('/pengunjung', 'pengunjung')->name('pengunjung');
        Route::get('/forms/pengunjung', 'pengunjung')->name('forms.pengunjung');
        Route::get('/peminjaman', 'peminjaman')->name('peminjaman');
    });


Route::controller(App\Http\Controllers\Service\AuthController::class)->prefix('auth')->group(function () {
    Route::get('login', 'loginPage')->middleware('guest')->name('login');
    Route::post('login', 'loginPost')->middleware('guest')->name('login.post');
    Route::post('logout', 'logout')->middleware('auth')->name('logout');
});
Route::middleware('auth')->prefix('admin')->as('admin.')->group(function () {
    Route::controller(App\Http\Controllers\Admin\PageController::class)->group(function () {
        Route::get('/', 'index')->name('dashboard');
    });
    Route::resource('lending', App\Http\Controllers\Admin\LendingController::class);
    Route::resource('guest', App\Http\Controllers\Admin\GuestController::class);
    Route::resource('items', App\Http\Controllers\Admin\ItemsController::class);

    Route::controller(App\Http\Controllers\ExportController::class)->group(function () {
        Route::get('/export/datatamu', 'exportDataTamu')->name('export.datatamu');
        Route::get('/export/datapeminjaman', 'exportDataPeminjaman')->name('export.peminjaman');
    });
});
