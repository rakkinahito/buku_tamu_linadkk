@extends('layouts.empty')

@section('title', 'Login Page')

@section('content')
    <div class="bg-primary d-flex w-100 justify-content-center align-items-center" style="height: 100vh">
        <div class="col-xl-3 col-lg-4">
            <div class="card card-body shadow">
                <h3 class="text-center mb-4">Halaman Masuk</h3>
                @include('alerts')
                <form action="{{ route('login.post') }}" method="post">
                    @csrf
                    <div class="form-floating mb-3">
                        <input type="text" name="username" class="form-control" id="floatingEmail" placeholder="achmad23">
                        <label for="floatingEmail">Nama Pengguna/Email</label>
                    </div>
                    <div class="form-floating mb-3">
                        <input type="password" name="password" class="form-control" id="floatingSandi"
                            placeholder="*******">
                        <label for="floatingSandi">Kata Sandi</label>
                    </div>
                    <button type="submit" class="btn btn-lg btn-primary w-100">
                        Masuk
                    </button>
                </form>
                <hr />
                <span class="text-center">
                    &copy; Team Kerja Praktik Widyatama
                </span>
            </div>
        </div>
    </div>
@endsection
