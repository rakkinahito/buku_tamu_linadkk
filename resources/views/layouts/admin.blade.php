<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    @stack('styles')

    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
</head>

<body>
    <main id="app">
        <header class="sticky-top shadow mb-3">
            <nav class="navbar navbar-expand bg-body-tertiary">
                <div class="container">
                    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">Admin | Buku Tamu</a>
                    <div class="navbar-nav">
                        <a class="nav-link pe-0" href="#"><em
                                class="bi bi-person-circle me-2"></em>{{ auth()->user()->name }}</a>
                        <div class="vr mx-3"></div>
                        <button type="button" class="btn btn-outline-danger btn-sm" data-laravel="logout">
                            <em class="bi bi-box-arrow-left"></em> Keluar
                        </button>
                        <form action="{{ route('logout') }}" method="post" data-laravel="logoutForm">
                            @csrf
                        </form>
                    </div>
                </div>
            </nav>
            <div class="navbar navbar-expand-lg bg-primary navbar-dark">
                <div class="container">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav">
                            <a class="nav-link ps-0" href="{{ route('admin.dashboard') }}"><em
                                    class="bi bi-columns-gap me-2"></em>Beranda</a>
                            <a class="nav-link" href="{{ route('admin.items.index') }}"><em
                                    class="bi bi-boxes me-2"></em>Data Fasilitas</a>
                            <a class="nav-link" href="{{ route('admin.guest.index') }}"><em
                                    class="bi bi-people me-2"></em>Data Tamu</a>
                            <a class="nav-link" href="{{ route('admin.lending.index') }}"><em
                                    class="bi bi-clipboard me-2"></em>Data Peminjaman</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="container">
            @yield('content')
        </section>
    </main>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
    <script>
        const logoutButton = document.querySelector("[data-laravel='logout']");
        const logoutForm = document.querySelector("[data-laravel='logoutForm']");

        logoutButton.addEventListener("click", function() {
            logoutForm.submit()
        })

        // Init DataTables
        let table = new DataTable('#table');
    </script>
    @stack('scripts')
</body>

</html>
