@extends('layouts.frontpage')

@section('title', 'Selamat Datang Di BBPVP Bandung Oleh Kementrian Ketenagakerjaan')


@section('content')
    @include('components.navbar-frontpage')
    <section class="row align-items-center mt-3">
        <div class="col-lg">
            <h1 class="mb-3">Selamat Datang Di BBPVP Bandung Oleh Kementrian Ketenagakerjaan</h1>
            <p class="mb-4">Tingkatkan Kompetensimu dengan mengikuti Pelatihan PBK Kejuruan Otomotif, Manufaktur,
                Refrigerasi, TIK dan
                Metodologi.</p>
            <a href="{{ route('menu.menu') }}" class="btn btn-light rounded-pill px-4">
                Get Started
            </a>
        </div>
        <div class="col-lg text-end">
            <img src="{{ asset('svg/home-image.svg') }}" alt="SIAPkerjaImg">
        </div>
    </section>

@endsection
