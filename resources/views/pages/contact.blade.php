@extends('layouts.frontpage')

@section('title', 'Kontak BBPVP')


@section('content')
    @include('components.navbar-frontpage')
    <h1 class="mb-3">Contact</h1>
    <section class="row align-items-center">
        <div class="col-lg">
            <div class="d-flex mb-3">
                <img src="{{ asset('svg/geoloc.svg') }}" alt="Geoloc" style="width: 50px;">
                <div class="ms-4">
                    <legend class="fw-bold">Alamat Kantor</legend>
                    <p class="mb-3 text-justify">JL. JENDERAL GATOT SUBROTO NO. 170 KELURAHAN GUMURUH KECAMATAN BATUNUNGGAL
                        KOTA BANDUNG</p>
                </div>
            </div>
            <div class="d-flex mb-3">
                <img src="{{ asset('svg/email.svg') }}" alt="Email" style="width: 50px;">
                <div class="ms-4">
                    <legend class="fw-bold">Email</legend>
                    <p class="mb-3 text-justify">
                        <a href="mailto:bbpvp.bandung@kemnaker.go.id"
                            class="text-white text-decoration-none">bbpvp.bandung@kemnaker.go.id</a>
                    </p>
                </div>
            </div>
            <div class="d-flex mb-3">
                <img src="{{ asset('svg/phone.svg') }}" alt="Phone" style="width: 50px;">
                <div class="ms-4">
                    <legend class="fw-bold">Call:</legend>
                    <p class="mb-3 text-justify">022-7312564</p>
                </div>
            </div>
        </div>
        <div class="col-lg">
            <img src="{{ asset('svg/contact-image.svg') }}" alt="SIAPkerjaImg">
        </div>
    </section>

@endsection
