@extends('layouts.frontpage')
@section('title', 'Ulasan Layanan')

@push('styles')
    <style>
        .rate {
            float: left;
            height: 56px;
            padding: 0 10px;
        }

        .rate:not(:checked)>input {
            position: absolute;
            top: -9999px;
        }

        .rate:not(:checked)>label {
            float: right;
            width: 1em;
            overflow: hidden;
            white-space: nowrap;
            cursor: pointer;
            font-size: 45px;
            color: #ccc;
        }

        .rate:not(:checked)>label:before {
            content: '★ ';
        }

        .rate>input:checked~label {
            color: #ffc700;
        }

        .rate:not(:checked)>label:hover,
        .rate:not(:checked)>label:hover~label {
            color: #deb217;
        }

        .rate>input:checked+label:hover,
        .rate>input:checked+label:hover~label,
        .rate>input:checked~label:hover,
        .rate>input:checked~label:hover~label,
        .rate>label:hover~input:checked~label {
            color: #c59b08;
        }

        /* Modified from: https://github.com/mukulkant/Star-rating-using-pure-css */
    </style>
@endpush


@section('content')
    <section class="d-flex justify-content-center flex-column">
        <form action="{{ route('page.ulasan.post', ['jenisUlasan' => $jenisUlasan]) }}" method="post">
            @csrf
            <div class="mb-3 d-flex flex-column justify-content-center align-items-center text-center">
                <legend>Kualitas Layanan</legend>
                <div class="rate" style="width: 250px">
                    <input type="radio" id="star5" name="rate" value="5" />
                    <label for="star5" title="text">5 stars</label>
                    <input type="radio" id="star4" name="rate" value="4" />
                    <label for="star4" title="text">4 stars</label>
                    <input type="radio" id="star3" name="rate" value="3" />
                    <label for="star3" title="text">3 stars</label>
                    <input type="radio" id="star2" name="rate" value="2" />
                    <label for="star2" title="text">2 stars</label>
                    <input type="radio" id="star1" name="rate" value="1" />
                    <label for="star1" title="text">1 star</label>
                </div>
            </div>
            <div class="mb-3 d-flex flex-column justify-content-center align-items-center text-center">
                <legend>Saran dan Kritik</legend>
                <textarea name="desc" id="desc" rows="3" class="form-control" placeholder="Berikan Ulasan Anda"></textarea>
            </div>
            <div class="mb-3  text-center">
                <a href="{{ route('page.home') }}" class="btn btn-light">
                    Kembali ke Beranda
                </a>
                <button type="submit" class="btn btn-primary me-1">
                    Simpan Ulasan
                </button>
            </div>
        </form>
    </section>
@endsection
