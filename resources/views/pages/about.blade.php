@extends('layouts.frontpage')

@section('title', 'Tentang BBPVP')


@section('content')
    @include('components.navbar-frontpage')
    <section class="row align-items-center mt-3">
        <div class="col-lg">
            <img src="{{ asset('svg/about-image.svg') }}" alt="SIAPkerjaImg">
        </div>
        <div class="col-lg">
            <h1 class="text-center mb-3">Tentang BBPVP Bandung</h1>
            <p class="mb-3 text-justify">Balai Besar Pelatihan Vokasi Dan Produktivitas (BBPVP) Bandung Adalah
                Lembaga Pelatihan Pemerintah Yang Merupakan Salah Satu Unit
                Pelaksana Teknis Pusat (UPTP) Dibawah Direktorat Jenderal Pembinaan
                Pelatihan Dan Produktivitas, Kementerian Ketenagakerjaan Republik
                Indonesia, Berdasarkan Peraturan Menteri Ketenagakerjaan Republik
                Indonesia Nomor 21 Tahun 2015. BBPLK Bandung Memiliki Tugas
                Melaksanakan Pengembanganpelatihan, Pemberdayaan, Dan Sertifikasi
                Tenaga Kerja, Instruktur, Dan Tenaga Pelatihan. BBPLK Diresmikan
                Pada 23 Februari 1952 Atas Inisiatif Pemerintah Republik Indonesia
                Bekerjasama Dengan Program Colombo Plan. BBPLK Terletak Di J
                alan Jenderal Gatot Subroto No. 170, Bandung, Dengan Luas Lahan
                Sekitar 3 Hektar.</p>
        </div>
    </section>

@endsection
