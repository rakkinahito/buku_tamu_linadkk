@extends('layouts.frontpage')

@section('title', 'Form Pengunjung')


@push('styles')
    @livewireStyles
@endpush

@push('scripts')
    @livewireScripts
@endpush

@section('content')
    <h1 class="text-center mb-5">Form Pengunjung</h1>
    @livewire('form-pengunjung')
@endsection
