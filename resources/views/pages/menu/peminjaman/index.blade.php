@extends('layouts.frontpage')

@section('title', 'Form Peminjaman Fasilitas')


@push('styles')
    @livewireStyles
@endpush

@push('scripts')
    @livewireScripts
@endpush

@section('content')
    <h1 class="text-center mb-5">Form Peminjaman Fasilitas</h1>
    @livewire('form-peminjaman')
@endsection
