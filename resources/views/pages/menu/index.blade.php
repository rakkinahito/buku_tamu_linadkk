@extends('layouts.frontpage')

@section('title', 'Kontak BBPVP')


@push('styles')
    <style>
        .img-fix-bot-right {
            position: fixed;
            height: 400px;
            bottom: 0;
            right: 0;
        }
    </style>
@endpush

@section('content')
    <section class="text-center d-flex flex-column">
        <h1 class="mb-5">Menu</h1>

        <div>
            <a href="{{ route('menu.pengunjung') }}" class="btn btn-light px-4 mb-4 rounded-pill">Pengunjung</a>
        </div>
        <div>
            <a href="{{ route('menu.peminjaman') }}" class="btn btn-light px-4 rounded-pill">Peminjaman Fasilitas</a>
        </div>
    </section>
    <img src="{{ asset('svg/menu-image.svg') }}" alt="Pilih Menu" class="d-xl-block d-none img-fix-bot-right">

@endsection
