<div>
    {{-- The Master doesn't talk, he acts. --}}
    @include('alerts')
    <h4>Daftar Peminjaman Fasilitas Hari Ini {{ now()->format('d M Y') }}</h4>
    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Fasilitas</th>
                    <th scope="col">Jam</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($this->peminjamanHariIni as $fasilitasHariIni)
                    <tr>
                        <th scope="row">{{ $loop->iteration }}</th>
                        <td>{{ $fasilitasHariIni->item->nama_barang }}</td>
                        <td>{{ $fasilitasHariIni->waktu_mulai }} s.d {{ $fasilitasHariIni->waktu_akhir }}</td>
                    </tr>
                @endforeach
                @if (count($this->peminjamanHariIni) < 1)
                    <tr>
                        <td colspan="3" class="text-center text-success fw-bold">
                            Fasilitas Tersedia semua
                        </td>
                    </tr>
                @endif
            </tbody>
        </table>
    </div>
    <hr />
    <form wire:submit.prevent="submitForm" method="post">
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="name">Nama</label>
            </div>
            <div class="col-lg">
                <input type="text" name="name" class="form-control" id="name" required placeholder="Lina"
                    wire:model.debounce.500ms="name">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="instansi">Instansi</label>
            </div>
            <div class="col-lg">
                <input type="text" name="instansi" class="form-control" id="instansi" required
                    placeholder="Nama Instansi" wire:model="instansi">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="no_hp">No. HP/WA</label>
            </div>
            <div class="col-lg">
                <input type="text" inputmode="numeric" name="no_hp" class="form-control" id="no_hp" required
                    placeholder="0812*********" wire:model.debounce.500ms="no_hp">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="alamat">Alamat</label>
            </div>
            <div class="col-lg">
                <input type="text" name="alamat" class="form-control" id="alamat" required
                    placeholder="Jl. Ujung Berung ..." wire:model.debounce.500ms="alamat">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="tanggal_peminjaman">Tanggal Peminjaman</label>
            </div>
            <div class="col-lg">
                <input type="date" name="tanggal_peminjaman" class="form-control" id="tanggal_peminjaman" required
                    placeholder="Nama" wire:model="tanggal_peminjaman">
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label>Waktu Peminjaman (06:00 s.d 18:00)</label>
            </div>
            <div class="col-lg">
                <div class="row align-items-center">
                    <div class="col-lg">
                        <input type="time" name="waktu_mulai" class="form-control" id="waktu_mulai" required
                            placeholder="Nama" wire:model="waktu_mulai" min="06:00">
                    </div>
                    <div class="col-lg-1 text-center">
                        <span>s.d.</span>
                    </div>
                    <div class="col-lg">
                        <input type="time" name="waktu_akhir" class="form-control" id="waktu_akhir" required
                            placeholder="Nama" wire:model="waktu_akhir" max="18:00">
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <label for="fasilitas_id">Fasilitas</label>
            </div>
            <div class="col-lg">
                <select name="fasilitas_id" id="fasilitas_id" class="form-select" wire:model="fasilitas_id">
                    <option value="none" selected>- Pilih Fasilitas -</option>
                    @foreach ($this->daftarBarang as $item)
                        <option value="{{ $item->id }}">{{ $item->nama_barang }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <hr />
        <button type="submit" class="btn btn-primary w-100">
            Simpan
        </button>
    </form>
</div>
