<div>
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
    @include('alerts')
    <form wire:submit.prevent="submitForm" method="post">
        <div class="row mb-3">
            <div class="col-lg-4 mb-lg-0 mb-3">
                <span class="form-label">Punya Akun SIAPkerja</span>
            </div>
            <div class="col-lg">
                <input type="radio" class="btn-check" name="punya_akun" wire:change="setPunyaAkun($event.target.value)"
                    value="YA" id="punya_akun_1" autocomplete="off" checked>
                <label class="btn btn-outline-light" for="punya_akun_1">Ya</label>

                <input type="radio" class="btn-check" name="punya_akun"
                    wire:change="setPunyaAkun($event.target.value)" value="TIDAK" id="punya_akun_2" autocomplete="off">
                <label class="btn btn-outline-secondary" for="punya_akun_2">Tidak</label>
            </div>
        </div>
        <div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="no_registrasi">No. Registrasi SIAPkerja</label>
                </div>
                <div class="col-lg">
                    <input type="text" name="no_registrasi" class="form-control" id="no_registrasi"
                        placeholder="No. Registrasi SIAPkerja" wire:model.debounce.500ms="no_registrasi" aut>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="name">Nama</label>
                </div>
                <div class="col-lg">
                    <input type="text" name="name" class="form-control" id="name" required
                        placeholder="Maria" wire:model.debounce.500ms="name">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="tujuan">Tujuan</label>
                </div>
                <div class="col-lg">
                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-select" wire:model="opsi_tujuan">
                        <option value="PELATIHAN" selected>Konsultasi Pelatihan</option>
                        <option value="SERTIFIKASI">Konsultasi Sertifikasi</option>
                        <option value="PASAR KERJA">Konsultasi Pasar Kerja dan Penempatan Tenaga Kerja</option>
                        <option value="JAMSOSTEK">Konsultasi Jaminan Sosial Tenaga Kerja</option>
                        <option value="KEWIRAUSAHAAN">Konsultasi Kewirausahaan/Perluas Kesempatan Kerja</option>
                        <option value="KESELAMATAN">Konsultasi Keselamatan dan Kesehatan Kerja</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="jenis_kelamin">Jenis Kelamin</label>
                </div>
                <div class="col-lg">
                    <select name="jenis_kelamin" id="jenis_kelamin" class="form-select" wire:model="jenis_kelamin">
                        <option value="PRIA" selected>PRIA</option>
                        <option value="WANITA">WANITA</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="usia">Usia</label>
                </div>
                <div class="col-lg">
                    <input type="number" name="usia" class="form-control" id="usia" required placeholder="25"
                        wire:model.debounce.500ms="usia">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="pendidikan">Pendidikan</label>
                </div>
                <div class="col-lg">
                    <select name="pendidikan" id="pendidikan" class="form-select" wire:model="pendidikan_terakhir">
                        <option value="SD/MI" selected>SD/MI Sederajat</option>
                        <option value="SMP/MTS" selected>SMP/MTS Sederajat</option>
                        <option value="SMK/SMA" selected>SMA/SMK Sederajat</option>
                        <option value="D1/D3">D1/D3</option>
                        <option value="S1">S1</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="domisili">Asal Domisili</label>
                </div>
                <div class="col-lg">
                    <select name="domisili" id="domisili" class="form-select" wire:model="domisili">
                        <option value="KOTA BANDUNG" selected>Kota Bandung</option>
                        <option value="KAB BANDUNG">Kab. Bandung</option>
                        <option value="LUAR BANDUNG">Luar Bandung</option>
                    </select>
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="email">Email</label>
                </div>
                <div class="col-lg">
                    <input type="email" name="email" class="form-control" id="email" required
                        placeholder="mymail@mail.com" wire:model.debounce.500ms="email">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="no_hp">No HP / WA</label>
                </div>
                <div class="col-lg">
                    <input type="text" name="no_hp" class="form-control" id="no_hp" required
                        placeholder="0896787*******" wire:model.debounce.500ms="no_hp">
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-lg-4 mb-lg-0 mb-3">
                    <label for="no_hp">Alumni BBPVP Bandung</label>
                </div>
                <div class="col-lg">
                    <select name="alumni" id="alumni" class="form-select" wire:model="alumni">
                        <option value="YA" selected>YA</option>
                        <option value="TIDAK">TIDAK</option>
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary w-100" @disabled($isPhotoUploaded)>Simpan</button>
    </form>
</div>
