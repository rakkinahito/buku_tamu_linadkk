@extends('layouts.admin')

@section('title', 'Daftar Fasillitas')

@section('content')
    <div class="card card-body shadow">
        <h3 class="text-primary mb-3">Daftar Fasillitas</h3>
        <div class="text-end mb-3">
            <a href="{{ route('admin.items.create') }}" class="btn btn-primary">
                <em class="bi bi-plus"></em> Tambah Fasillitas
            </a>
        </div>
        @include('alerts')
        <div class="table-responsive">
            <table class="table table-striped table-hovered" id="table">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Nama Fasillitas</th>
                        <th scope="col">Penginput</th>
                        <th scope="col">Tanggal Dibuat</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($barang as $b)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $b->nama_barang }}</td>
                            <td>{{ $b->penginput }}</td>
                            <td>{{ $b->created_at }}</td>
                            <td class="d-flex">
                                <a href="{{ route('admin.items.show', ['item' => $b->id]) }}"
                                    class="btn btn-sm btn-outline-info me-1">
                                    <em class="bi bi-eye"></em>
                                </a>
                                <form action="{{ route('admin.items.destroy', ['item' => $b->id]) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger">
                                        <em class="bi bi-trash"></em>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
