@extends('layouts.admin')

@section('title', 'Form Tambah Data Fasillitas')

@section('content')
    <div class="card card-body shadow">
        <legend class="text-primary mb-3">Form Tambah Data Fasillitas</legend>
        <form action="{{ route('admin.items.store') }}" method="post">
            @csrf
            <input type="hidden" value="{{ auth()->user()->name }}" name="penginput">
            <div class="row mb-3">
                <div class="col-lg-4 mb-sm-3 mb-lg-0">
                    <label for="nama_barang">Nama Fasillitas</label>
                </div>
                <div class="col-lg">
                    <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Ruang Aula"
                        required>
                </div>
            </div>
            <input type="hidden" name="kuantitas" value="1">
            <hr />
            <div class="row">
                <div class="col">
                    <a href="{{ route('admin.items.index') }}" class="btn btn-outline-secondary w-100">
                        Kembali
                    </a>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary w-100">
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
