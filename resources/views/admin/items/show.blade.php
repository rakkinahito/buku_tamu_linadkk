@extends('layouts.admin')

@section('title', 'Detail Barang')

@section('content')
    <div class="card card-body shadow">
        <h3 class="text-primary">Detail Fasilitas</h3>

        <form action="{{ route('admin.items.update', ['item' => $barang->id]) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row mb-3">
                <div class="col-lg-4 mb-sm-3 mb-lg-0">
                    <label for="nama_barang">Nama Fasilitas</label>
                </div>
                <div class="col-lg">
                    <input type="text" name="nama_barang" id="nama_barang" value="{{ $barang->nama_barang }}"
                        class="form-control" placeholder="Sapu" required>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <a href="{{ route('admin.items.index') }}" class="btn btn-outline-secondary w-100">
                        Kembali
                    </a>
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary w-100">
                        Simpan
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection
