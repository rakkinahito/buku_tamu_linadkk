@extends('layouts.admin')

@section('title', 'Daftar Tamu')

@section('content')
    <div class="card card-body shadow">
        <div class="row">
            <div class="col-lg">
                <h3 class="text-primary mb-3">Daftar Tamu</h3>
            </div>
            <div class="col-lg-2 text-end">
                <a href="{{ route('admin.export.datatamu') }}" target="_blank" rel="noopener noreferrer"
                    class="btn btn-sm btn-outline-info">
                    <em class="bi bi-filetype-xlsx me-2"></em> Export Data Tamu
                </a>
            </div>
        </div>
        @include('alerts')
        <div class="table-responsive">
            <table class="table table-striped table-hovered" id="table">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Punya Akun SIAPkerja</th>
                        <th scope="col">Domisili</th>
                        <th scope="col">Alumni BBPVP</th>
                        <th scope="col">Tanggal Daftar</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($guests as $b)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $b->nama }}</td>
                            <td>{{ $b->punya_akun }}</td>
                            <td>{{ $b->domisili }}</td>
                            <td>
                                {{ $b->alumni_bbpvp }}
                            </td>
                            <td>{{ $b->created_at }}</td>
                            <td class="d-flex">
                                <form action="{{ route('admin.guest.destroy', ['guest' => $b->id]) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger">
                                        <em class="bi bi-trash"></em>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
