@extends('layouts.admin')

@section('title', 'Daftar Peminjaman Barang')

@section('content')
    <div class="card card-body shadow">
        <div class="row">
            <div class="col-lg">
                <h3 class="text-primary mb-3">Daftar Peminjaman Barang</h3>
            </div>
            <div class="col-lg-3 text-end">
                <a href="{{ route('admin.export.peminjaman') }}" target="_blank" rel="noopener noreferrer"
                    class="btn btn-sm btn-outline-info">
                    <em class="bi bi-filetype-xlsx me-2"></em> Export Data Peminjaman
                </a>
            </div>
        </div>
        @include('alerts')
        <div class="table-responsive">
            <table class="table table-striped table-hovered" id="table">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Nama</th>
                        <th scope="col">No. Ponsel</th>
                        <th scope="col">Fasilitas</th>
                        <th scope="col">Tanggal Pinjam</th>
                        <th scope="col">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($lendings as $b)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $b->name }}</td>
                            <td>{{ $b->no_hp }}</td>
                            <td>{{ $b?->item->nama_barang }}</td>
                            <td>{{ $b->tanggal_peminjaman }}</td>
                            <td class="d-flex">
                                <form action="{{ route('admin.lending.destroy', ['lending' => $b->id]) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-sm btn-outline-danger">
                                        <em class="bi bi-trash me-2"></em> Hapus
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
