<table>
    <thead>
        <tr>
            <th scope="col">No.</th>
            <th scope="col">Nama</th>
            <th scope="col">No. Ponsel</th>
            <th scope="col">Fasilitas</th>
            <th scope="col">Tanggal Pinjam</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($lendings as $b)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $b->name }}</td>
                <td>{{ $b->no_hp }}</td>
                <td>{{ $b?->item->nama_barang }}</td>
                <td>{{ $b->tanggal_peminjaman }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
