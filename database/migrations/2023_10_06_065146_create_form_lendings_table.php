<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('form_lendings', function (Blueprint $table) {
            $table->id();
            $table->string("name");
            $table->string("alamat");
            $table->char('no_hp', 12)->unique();
            $table->date('tanggal_peminjaman');
            $table->date('tanggal_pengembalian');
            $table->string('fasilitas');
            $table->foreignId('item_id')->constrained('items')->cascadeOnDelete()->cascadeOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('form_lendings');
    }
};
