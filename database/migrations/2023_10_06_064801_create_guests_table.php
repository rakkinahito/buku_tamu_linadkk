<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->id();
            $table->string("nama")->nullable();
            $table->string("opsi_tujuan")->nullable();
            $table->string("tujuan_desc")->nullable();
            $table->enum("jenis_kelamin", ['PRIA', 'WANITA'])->nullable();
            $table->enum('pendidikan_terakhir', ["SD/MI", 'S1', 'D1/D3', 'SMK/SMA', "SMP/MTS"])->nullable();
            $table->string('domisili')->nullable();
            $table->string('email');
            $table->char('no_hp', 12)->nullable();
            $table->tinyInteger('usia')->unsigned()->nullable();
            $table->string("no_registrasi")->nullable();
            $table->enum("alumni_bbpvp", ["YA", "TIDAK"])->nullable();
            $table->enum("punya_akun", ["YA", "TIDAK"])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guests');
    }
};
