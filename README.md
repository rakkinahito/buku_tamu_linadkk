# Cara Pakai

1. Pertama Pastiin udah install <strong>XAMPP PHP 8.1, Composer, Git CLI</strong>
2. Aktifin XAMPP yang Apache dan MySQL
3. Clone / Download Project ini
4. Buka Foldernya pake File Explorer, lalu klik kanan diarea yang kosong. dan pilih Open GIT Bash Here.
5. lalu jalani command dibawah ini.

```bash
composer install

php artisan serve
```

6. Lalu untuk database, bikin database barunya di <a href="http://localhost/phpmyadmin">PHP MY ADMIN</a> dengan nama <strong>buku_tamu<strong>
7. Lalu pergi ke tab import dan import file .sql yang diberi, jangan lupa download dulu dari whatsapp yang sudah dikasih. lalu klik oke.
8. Kalau udah nanti buka <a href="http://localhost:8000/">Local Host</a> dan aplikasi running.

# Akun Admin :

username : admin
pass: 1234

#### Coded By : Rakkina Hito
