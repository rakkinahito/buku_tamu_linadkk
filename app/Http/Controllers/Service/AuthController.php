<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    //

    public function loginPage()
    {
        return view('auth.login');
    }

    public function loginPost(Request $request)
    {
        $loginDataValidation = Validator::make($request->all(), [
            'username' => ['required'],
            'password' => ['required']
        ]);

        if ($loginDataValidation->fails()) {
            return redirect()->route('admin.dashboard')->withErrors($loginDataValidation->errors());
        }

        $credentials = $loginDataValidation->validated();

        $userData = User::where('username', $credentials['username'])->orWhere('email', $credentials['username'])->first();

        if (\is_null($userData)) {
            # Kalau data user Kosong
            return redirect()->route('login')->with('error', 'Email/Nama Pengguna tidak Terdaftar.');
        }

        if (!Hash::check($credentials['password'], $userData->password)) {
            return redirect()->route('login')->with('error', 'Kata Sandi tidak sesuai.');
        }

        auth()->login($userData);

        return redirect()->route('admin.dashboard')->with('success', 'Proses Autentikasi Berhasil');
    }

    public function logout()
    {
        \auth()->logout();
        Session::flush();

        return redirect()->route('admin.dashboard')->with('success', 'Proses Keluar Berhasil');
    }
}
