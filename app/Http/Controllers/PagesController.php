<?php

namespace App\Http\Controllers;

use App\Models\Review;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    //
    public function index()
    {
        return view('pages.home');
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function ulasan(Request $request, $jenisUlasan)
    {
        if (!\in_array($jenisUlasan, ["peminjaman", 'bukutamu'])) {
            return redirect()->route('page.home')->with('error', 'Jenis ulasan tidak terdaftar');
        }

        return view('pages.ulasan', [
            'jenisUlasan' => $jenisUlasan
        ]);
    }

    public function ulasanPost(Request $request, $jenisUlasan)
    {
        if (!\in_array($jenisUlasan, ["peminjaman", 'bukutamu'])) {
            return redirect()->back()->with('error', 'Jenis ulasan tidak terdaftar');
        }

        Review::create([
            'jenis_pelaayanan' => $jenisUlasan,
            'score' => $request->post('rate'),
            'desc' => $request->post('desc')
        ]);

        return redirect()->route('page.home')->with('success', 'Terima Kasih');
    }
}
