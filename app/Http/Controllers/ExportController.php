<?php

namespace App\Http\Controllers;

use App\Exports\DataPeminjamanExport;
use Illuminate\Http\Request;

use App\Exports\DataTamuExport;
use Maatwebsite\Excel\Excel;

class ExportController
{
    private $excel;

    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }

    public function exportDataTamu()
    {
        return $this->excel->download(new DataTamuExport, 'data_tamu.xlsx');
    }

    public function exportDataPeminjaman()
    {
        return $this->excel->download(new DataPeminjamanExport, 'data_peminjaman.xlsx');
    }
}
