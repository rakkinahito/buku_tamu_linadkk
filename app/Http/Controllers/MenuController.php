<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    //
    public function menu()
    {
        return view('pages.menu.index');
    }

    public function peminjaman()
    {
        return view('pages.menu.peminjaman.index');
    }

    public function pengunjung()
    {
        return view('pages.menu.pengujung.index');
    }
}
