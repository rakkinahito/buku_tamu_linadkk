<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return view('admin.items.index', [
            'barang' => Item::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('admin.items.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama_barang' => 'required',
        ]);

        Item::create($request->post());

        return redirect()->route('admin.items.index')->with('success', 'Data Barang berhasil disimpan');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $barang = Item::find($id);

        if (\is_null($barang)) {
            return redirect()->back()->with('error', 'Data Barang tidak terdaftar');
        }

        return view('admin.items.show', \compact('barang'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        return redirect()->route('admin.items.show', ['item' => $id]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $barang = Item::find($id);

        $request->validate([
            'nama_barang' => 'required',
        ]);

        if (\is_null($barang)) {
            return redirect()->back()->with('error', 'Data Barang tidak terdaftar');
        }

        $barang->nama_barang = $request->post('nama_barang');
        $barang->penginput = auth()->user()->name;

        $barang->save();

        return redirect()->route('admin.items.index')->with('success', 'Data Barang berhasil di audit.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        Item::destroy($id);
        return redirect()->route('admin.items.index')->with('success', 'Data Barang berhasil dihapus');
    }
}
