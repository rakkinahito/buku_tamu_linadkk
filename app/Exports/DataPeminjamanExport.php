<?php

namespace App\Exports;

use App\Models\FormLending;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class DataPeminjamanExport implements FromView
{
    use Exportable;

    public function view(): View
    {
        return view('admin.exports.peminjaman', [
            'lendings' => FormLending::with(['item'])->get()
        ]);
    }
}
