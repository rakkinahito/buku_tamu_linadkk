<?php

namespace App\Exports;

use App\Models\Guest;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class DataTamuExport implements FromCollection
{
    use Exportable;

    public function collection()
    {
        return Guest::all();
    }
}
