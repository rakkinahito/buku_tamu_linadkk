<?php

namespace App\Livewire;

use App\Models\FormLending;
use App\Models\Item;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

use Illuminate\Support\Str;
use Livewire\Attributes\Computed;

class FormPeminjaman extends Component
{

    // Data Peminjaman
    public string $name, $no_hp, $alamat, $tanggal_peminjaman, $fasilitas_id, $instansi, $waktu_mulai, $waktu_akhir;

    public function mount()
    {
        $this->name = "";
        $this->no_hp = "";
        $this->alamat = "";
        $this->tanggal_peminjaman = "";
        $this->fasilitas_id = "";
        $this->instansi = "";
        $this->waktu_mulai = "06:00";
        $this->waktu_akhir = "18:00";
    }

    #[Computed]
    public function peminjamanHariIni()
    {
        return FormLending::with('item')->where('tanggal_peminjaman', now()->toDateString())->get();
    }

    public function getDaftarBarangProperty()
    {
        return Item::all();
    }

    public function submitForm()
    {
        $cekKetersediaanFasilitas = FormLending::where(
            [
                'tanggal_peminjaman' => $this->tanggal_peminjaman,
                'waktu_mulai' => $this->waktu_mulai,
                'item_id' => $this->fasilitas_id
            ]
        )->first();

        if (!is_null($cekKetersediaanFasilitas) || $this->fasilitas_id == "none") {
            # Kalau ada Fasilitas yang dipinjam pada tanggal itu maka ditolak
            session()->flash('error', 'Mohon maaf pada tanggal tersebut Fasilitas telah di booking.');
        } else {
            FormLending::insert(
                [
                    'name' => $this->name,
                    'no_hp' => $this->no_hp,
                    'alamat' => $this->alamat,
                    'tanggal_peminjaman' => $this->tanggal_peminjaman,
                    'item_id' => $this->fasilitas_id,
                    'created_at' => now()->toDateTimeString(),
                    'waktu_mulai' => $this->waktu_mulai,
                    'waktu_akhir' => $this->waktu_akhir,
                    'instansi' => $this->instansi
                ]
            );

            return redirect()->route('page.ulasan', ['jenisUlasan' => 'peminjaman'])->with('success', 'Data Peminjaman Barang berhasil di simpan');
        }
    }
}
