<?php

namespace App\Livewire;

use App\Models\Guest;
use Livewire\Component;

use Livewire\WithFileUploads;

use Illuminate\Support\Str;

class FormPengunjung extends Component
{

    use WithFileUploads;

    public $photo;

    public bool $isPhotoUploaded = false;

    public string $punya_akun;
    public string $name, $opsi_tujuan, $jenis_kelamin, $pendidikan_terakhir, $domisili, $email, $no_hp, $no_registrasi, $alumni;

    public function mount()
    {
        $this->name = "";
        $this->opsi_tujuan = "";
        $this->jenis_kelamin = "";
        $this->pendidikan_terakhir = "";
        $this->domisili = "";
        $this->email = "";
        $this->no_hp = "";
        $this->no_registrasi = "";
        $this->alumni = "";
        $this->punya_akun = "YA";
    }

    public function updatingPhoto()
    {
        $this->isPhotoUploaded = true;
    }

    public function updatedPhoto()
    {
        $this->isPhotoUploaded = false;
    }

    public function setPunyaAkun($val)
    {
        $this->punya_akun = $val;
    }

    public function submitForm()
    {
        Guest::insert([
            'nama' => $this->name,
            'no_registrasi' => $this->no_registrasi ?? null,
            'opsi_tujuan' => $this->opsi_tujuan,
            'jenis_kelamin' => $this->jenis_kelamin ?? "PRIA",
            'pendidikan_terakhir' => $this->pendidikan_terakhir ?? "SD/MI",
            'domisili' => $this->domisili ?? 'KOTA BANDUNG',
            'email' => $this->email,
            'no_hp' => $this->no_hp,
            'punya_akun' => $this->punya_akun ?? "YA",
            'alumni_bbpvp' => $this->alumni ?? "YA",
            'created_at' => now()->toDateTimeString()
        ]);

        return redirect()->route('page.ulasan', ['jenisUlasan' => 'bukutamu'])->with('success', 'Data Formulir Telah tersimpan dalam buku tamu');
    }
}
